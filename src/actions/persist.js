
export const SET_PERSONAL = 'SET_PERSONAL';
export const SET_DOB = 'SET_DOB';
export const SET_PERMISSION = 'SET_PERMISSION';
export const SET_PRESIST_ROUTE = 'SET_PRESIST_ROUTE';
export const CLEAR_ALL = 'CLEAR_ALL';

export const setPersonal = data => ({
  type: SET_PERSONAL,
  data,
});

export const setDob = data => ({
  type: SET_DOB,
  data
});

export const setPermission = data => ({
  type: SET_PERMISSION,
  data
});

export const clearAll = () => ({
  type: CLEAR_ALL,
});