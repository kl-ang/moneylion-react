import * as register from './register';
import * as persist from './persist';

export default {
    ...register,
    ...persist,
}