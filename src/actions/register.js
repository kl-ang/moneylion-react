
export const REGISTER = 'REGISTER';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAIL = 'REGISTER_FAIL';

export const register = (callback) => ({
  type: REGISTER,
  callback,
});

export const registerSuccess = data => ({
  type: REGISTER_SUCCESS,
  data,
});

export const registerFail = error => ({
  type: REGISTER_FAIL,
  error,
});
