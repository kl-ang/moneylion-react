import api from './api';

export default {
  register: (data) =>
    api.post('https://5f79819fe402340016f93139.mockapi.io/api/user', data),
};