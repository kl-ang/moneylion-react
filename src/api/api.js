const fetchUrl = (method, endpoint, body = {}) => {
    const parameters = {
        method: method,
        headers: {
            body: JSON.stringify(body)
        },
    };
    return fetch(endpoint, parameters)
        .then(response => {
            return response.json();
        })
        .then(json => {
            return json;
        });
  };
  
const api = {
    post(endpoint, params) {
      return fetchUrl('post', endpoint, params);
    },
  };
  
  export default api;