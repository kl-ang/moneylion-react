import React, { Component } from 'react';
import './App.css';
import { Header } from './components';
import { getStorage } from './utils/persistStorage';
import { isEmpty } from './utils/common';
import Welcome from './pages/registration/welcome';
import Personal from './pages/registration/personal';
import Dob from './pages/registration/dob';
import Agreement from './pages/registration/agreement';
import {
  BrowserRouter as Router,
  Route,
  Redirect
} from 'react-router-dom';

class App extends Component {
  render() {
    let redirectToUrl;
    if (isEmpty(getStorage('dob'))) {
      redirectToUrl = <Redirect to="/agreement" />
    } else if (isEmpty(getStorage('personal'))) {
      redirectToUrl = <Redirect to="/dob" />
    }

    return (
      <Router>
        <div className="App">
        {redirectToUrl}
            <Header />
            <Route exact path="/" component={Welcome} />
            <Route exact path="/welcome" component={Welcome} />
            <Route exact path="/personal" component={Personal} />
            <Route exact path="/dob" component={Dob} />
            <Route exact path="/agreement" component={Agreement} />
        </div>
      </Router>
    );
  }
}

export default App;
