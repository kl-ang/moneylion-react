import React, { Component } from 'react';
import './welcome.css';
import { Link } from 'react-router-dom';

class Welcome extends Component {
    render() {
        return (
            <div className="welcome">
                <h1>Welcome to MoneyLion</h1>
                <p>You are required to create a single page application using ReactJs framework to onboard users to the MoneyLion Account.
                </p>
                <Link className="btn btn-primary center" to={'/personal'}><strong>APPLY NOW</strong></Link>
            </div>
        )
    }
}

export default Welcome;

