import React, { Component } from 'react';
import './personal.css';
import { connect } from 'react-redux';
import Actions from '../../../actions';
import { Field, reduxForm } from 'redux-form';
import TextField from '../../../components/input/text-field';

class Personal extends Component {

    onSubmitPersonal = event => {
        event.preventDefault();

        let firstName = event.target.firstName.value;
        let lastName = event.target.lastName.value;
        let email = event.target.email.value;

        this.props.setPersonalInfo({firstName, lastName, email});

        this.props.history.push('/dob');
    }

    render() {
        return (
            <div className="personal">
                <h2>Create Your Account</h2>
                <form onSubmit={this.onSubmitPersonal}>
                    <div className="form-group">
                        <Field label="First Name" component={TextField} name="firstName" className="form-control" id="firstName" placeholder="First Name" required />
                    </div>
                    <div className="form-group">
                        <Field label="Last Name" component={TextField} name="lastName" className="form-control" id="lastName" placeholder="Last Name" required />
                    </div>
                    <div className="form-group">
                        <Field label="Email" component={TextField} name="email" className="form-control" id="email" placeholder="Email" required />
                    </div>
                    <div className="pt-2">
                        <button type="submit" className="btn btn-primary">CONIINUE</button>
                    </div>
                </form>
            </div>
        )
    }
}

const mapStateToProps = () => ({
    initialValues: {
        firstName: '',
        lastName: '',
        email: ''
    }
});

const mapDispatchToProp = {
    setPersonalInfo: Actions.setPersonal,
}

const PersonalForm = reduxForm({
    form: 'personal',
    destroyOnUnmount: false,
})(Personal);

export default connect(mapStateToProps, mapDispatchToProp)(PersonalForm);

