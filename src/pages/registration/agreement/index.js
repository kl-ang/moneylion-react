import React, { Component } from 'react';
import './agreement.css';
import { connect } from 'react-redux';
import Actions from '../../../actions';
import { Field, reduxForm } from 'redux-form';
import Checkbox from '../../../components/input/checkbox';

class Agreement extends Component {

    onSubmitAgreement = (event) => {
        event.preventDefault();
        this.props.register(this.onLoginSuccess);
    }

    onLoginSuccess = () => {
      alert('Registration Successful!');
      this.props.history.push('/welcome');
    }

    render() {
        return (
            <div className="agreement">
                <h2>One last step!</h2>
                <form onSubmit={this.onSubmitAgreement}>
                    <div className="form-group">
                        <Field label="I agree to the" component={Checkbox} name="agreement1" className="form-control" id="agreement1" />
                        <a href="https://www.moneylion.com/">Electronic Transaction Esign Consent</a>
                    </div>
                    <div className="form-group">
                        <Field label="I agree to the following agreements:" component={Checkbox} name="agreement2" className="form-control" id="agreement2" />
                        <a href="https://www.moneylion.com/">Privacy Notice</a>
                        <a href="https://www.moneylion.com/">Terms and Conditions</a>
                        <a href="https://www.moneylion.com/">Membership Agreementt</a>
                    </div>
                    <div className="pt-2">
                        <button type="submit" className="btn btn-primary">AGREE & CREATE ACCOUNT</button>
                    </div>
                </form>
            </div>
        )
    }
}

const mapStateToProps = () => ({
    initialValues: {
        agreement1: false,
        agreement2: false
    }
});

const mapDispatchToProps = {
    register: Actions.register,
}

const AgreementForm = reduxForm({
    form: 'agreement',
    destroyOnUnmount: false,
})(Agreement);

export default connect(mapStateToProps, mapDispatchToProps)(AgreementForm);

