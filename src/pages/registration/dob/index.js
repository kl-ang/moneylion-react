import React, { Component } from 'react';
import './dob.css';
import { connect } from 'react-redux';
import Actions from '../../../actions';
import { Field, reduxForm } from 'redux-form';
import TextField from '../../../components/input/text-field';

class Dob extends Component {

    onSubmitDob = event => {
        event.preventDefault();

        let dob = event.target.dob.value;
        this.props.setDobInfo({dob});

        this.props.history.push('/agreement');
    }

    render() {
        return (
            <div className="dob">
                <h2>What's your date of birth?</h2>
                <form onSubmit={this.onSubmitDob}>
                    <div className="form-group">
                        <Field label="Your Birthday" component={TextField} name="dob" className="form-control" id="dob" placeholder="MM/DD/YYYY" required />
                    </div>
                    <div className="pt-2">
                        <button type="submit" className="btn btn-primary">CONIINUE</button>
                    </div>
                </form>
            </div>
        )
    }
}

const mapStateToProps = () => ({
    initialValues: {
        dob: '',
    }
});

const mapDispatchToProp = {
    setDobInfo: Actions.setDob,
}

const DodForm = reduxForm({
    form: 'dob',
    destroyOnUnmount: false,
})(Dob);

export default connect(mapStateToProps, mapDispatchToProp)(DodForm);

