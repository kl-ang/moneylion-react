import { select, put, call, takeLatest, all, fork } from 'redux-saga/effects';
import Actions from '../actions';
import Api from '../api'
import { getStorage } from '../utils/persistStorage';
import { reset } from 'redux-form';

const getPersonalValues = store => store.form.personal.values;
const getDOBValues = store => store.form.dob.values;
const getAgreementValues = store => store.form.agreement.values;

function* register({callback}) {
  let personalValues = getStorage('personal');
  let dobValues = getStorage('dob');
  let agreementValues = {};

  if (personalValues === null) {
    personalValues = yield select(getPersonalValues); //Get redux form personal value
  }

  if (dobValues === null) {
    dobValues = yield select(getDOBValues); //Get redux form dob value
  }

  agreementValues = yield select(getAgreementValues);

  const data = {
    ...personalValues,
    ...dobValues,
    ...agreementValues
  };

  try {
    const response = yield call(Api.register, data);

    if (response && response.success) {
      yield put(Actions.registerSuccess(response));
      yield put(Actions.clearAll());
      yield call(callback);

      // Reset Redux form
      yield put(reset('agreement'));
      yield put(reset('dob'));
      yield put(reset('personal'));
    } else {
      yield put(Actions.registerFail(response));
    }
  } catch (err) {
    yield put(Actions.registerFail(err));
  }
}

function* watchRegister() {
  yield takeLatest(Actions.REGISTER, register);
}

export default function* registration() {
  yield all([
    fork(watchRegister),
  ]);
}