import { fork, all } from 'redux-saga/effects';
import register from './register';

export default function* root() {
  yield all([
    fork(register),
  ]);
}