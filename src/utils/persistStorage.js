import { getStore } from '../stores/configureStore';

//To retrieve storage data
export const getStorage = (info) => {
    const store = getStore();
    const state = store.getState();
    const value = state.persist[info];
    if (value) {
        return value;
    }
    return null;
};