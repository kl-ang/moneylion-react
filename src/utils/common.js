//To check valie is object
export const isEmpty = (obj) => {
    return Object.keys(obj).length !== 0;
};