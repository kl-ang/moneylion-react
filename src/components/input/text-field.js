
import React from 'react';

const TextField = (props) => {
    const { input: { value, name, onChange }, placeholder, required } = props;
    return (
        <div className="input-row">
            <label htmlFor={props.name} >{props.label}</label>
            <input 
                type="text" 
                name={name} 
                value={value} 
                placeholder={placeholder} 
                onChange={event => onChange(event.target.value)}
                required={required}
            />
            {props.meta.touched && props.meta.error &&
            <span className="error">{props.meta.error}</span>}
        </div>
    );
}

export default TextField;