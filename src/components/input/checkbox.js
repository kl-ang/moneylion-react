
import React from 'react';

const Checkbox = (props) => {
    const { input: { value, name, onChange } } = props;
    return (
        <div className="input-row">
            <input 
                type="checkbox" 
                name={name} 
                value={value} 
                onChange={event => onChange(!value)}
            />
             <label htmlFor={props.name} >{props.label}</label>
            {props.meta.touched && props.meta.error &&
            <span className="error">{props.meta.error}</span>}
        </div>
    );
}

export default Checkbox;