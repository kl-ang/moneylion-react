import React, { Component } from 'react';

class Header extends Component {
    render() {
        return (
            <header style={{borderBottom: "2px solid #2ECFBF"}}>
                <img src="logo.png" alt="logo" width="250px"/>
            </header>
        );
    }
}

export default Header;
