import Actions from '../actions';
import { combineReducers } from 'redux';

function personal(state = {}, action) {
  switch (action.type) {
    case Actions.SET_PERSONAL:
      return { firstName: action.data.firstName, lastName: action.data.lastName, email: action.data.email };
    case Actions.CLEAR_ALL:
      return {};
    default:
      return state;
  }
};

function dob(state = {}, action) {
  switch (action.type) {
    case Actions.SET_DOB:
      return { dob: action.data.dob };
    case Actions.CLEAR_ALL:
      return {};
    default:
      return state;
  }
};

function permission(state = {}, action) {
  switch (action.type) {
    case Actions.SET_PERMISSION:
      return action.data;
    case Actions.CLEAR_ALL:
      return {};
    default:
      return state;
  }
};

export default combineReducers({
    personal,
    dob,
    permission,
})