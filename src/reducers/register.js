import Actions from '../actions';

function register(state = { error: {}, data: {} }, action) {  
  switch (action.type) {
    case Actions.REGISTER:
      return {
        errors: {},
        data: {}
      };
    case Actions.REGISTER_SUCCESS:
      return {
        errors: {},
        data: action.data,
      };
    case Actions.REGISTER_FAIL:
      return {
        error: action.error,
        data: {},
      };
    default:
      return state;
  }
}

export default register;