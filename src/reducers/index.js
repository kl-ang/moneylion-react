import register from './register';
import persist from './persist';
import { reducer as formReducer } from 'redux-form';

export default {
    register,
    persist,
    form: formReducer,
};
