import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { persistStore, persistCombineReducers } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web
import rootReducer from '../reducers';
import rootSaga from '../sagas';
import { createLogger } from 'redux-logger';

const config = {
  key: 'root',
  storage,
  whitelist: ['persist'], // Only for presist reducer
};

let store;
const reducer = persistCombineReducers(config, rootReducer);
const sagaMiddleware = createSagaMiddleware();
const excludedActions = [];
const logger = createLogger({
  collapsed: true,
  predicate: (getState, action) => excludedActions.indexOf(action.type) < 0,
});

const middlewares = applyMiddleware(sagaMiddleware, logger);

export const getStore = () => store;

const configureStore = () => {
  store = createStore(reducer, middlewares);
  let persistor = persistStore(store);
  sagaMiddleware.run(rootSaga);
  return { persistor, store };
};

export default configureStore;